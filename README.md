# TBlock filter list repository mirror

The filter lists available in the [filter list repository index](https://tblock.codeberg.page/repository) are mirrored here, as long as the license allows us to do so.

All licenses can be found here: [https://tblock.codeberg.page/repository](https://tblock.codeberg.page/repository).
